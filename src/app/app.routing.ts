import {Routes} from '@angular/router';
import {DashboardComponent} from './pages/platform/dashboard/dashboard.component';
import {PlatformLayoutComponent} from './@core/layouts/platform/platform-layout.component';
import {PublicLayoutComponent} from './@core/layouts/public/public-layout.component';
import {LoginComponent} from './pages/public/login/login.component';
import {RegisterComponent} from './pages/public/register/register.component';
import {RecoveryPasswordComponent} from './pages/public/recovery-password/recovery-password.component';
import {UserListComponent} from './pages/platform/user-list/user-list.component';
import {AuthGuard} from './guards/AuthGuard';
import {UserDetailComponent} from './pages/platform/user-detail/user-detail.component';
import {ResetPasswordComponent} from './pages/public/reset-password/reset-password.component';
import {AdminGuard} from './guards/AdminGuard';
import {PolicyComponent} from './pages/public/policy/policy.component';

export const AppRoutes: Routes = [
  /**
   * put here all public routes.
   */
  {
    path: 'login',
    component: PublicLayoutComponent,
    children: [{
      path: '',
      component: LoginComponent
    }]
  }, {
    path: 'recovery-password',
    component: PublicLayoutComponent,
    children: [{
      path: '',
      component: RecoveryPasswordComponent
    }]
  }, {
    path: 'reset-password',
    component: PublicLayoutComponent,
    children: [{
      path: '',
      component: ResetPasswordComponent
    }]
  }, {
    path: 'register',
    component: PublicLayoutComponent,
    children: [{
      path: '',
      component: RegisterComponent
    }]
  }, {
    path: 'privacy-and-cookies-policy',
    component: PublicLayoutComponent,
    children: [{
      path: '',
      component: PolicyComponent
    }]
  }, {
    path: '',
    canActivate: [AuthGuard],
    component: PlatformLayoutComponent,
    /**
     * put as children all the routes that need an authenticated user.
     */
    children: [{
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    }, {
      path: 'dashboard',
      component: DashboardComponent
    }, {
      canActivate: [AdminGuard],
      path: 'user/:id',
      component: UserDetailComponent
    }, {
      path: 'profile',
      component: UserDetailComponent
    }, {
      canActivate: [AdminGuard],
      path: 'user-list',
      component: UserListComponent
    }]
  }, {
    /**
     * redirect all incorrect routes to dashboard.
     */
    path: '**',
    redirectTo: 'dashboard'
  }];
