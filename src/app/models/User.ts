import {UserRole} from '../services/session.service';
import {Mappable} from './interface/Mappable';
import {ServerUser} from '../server/models/ServerUser';

export class User implements Mappable<User> {
  id: string;
  userRole?: UserRole;
  firstName?: string;
  lastName?: string;
  description?: string;
  email?: string;
  telephone?: string;
  password?: string;
  dateOfBirth?: Date;
  registeredAt?: Date;
  profileImg?: string;
  fullName?: string;
  isMale?: boolean;
  media?: any[];

  constructor()
  constructor(id?: string,
              email?: string,
              password?: string,
              role?: UserRole,
              firstName?: string,
              lastName?: string,
              description?: string,
              telephone?: string,
              profileImg?: string,
              dateOfBirth?: Date,
              isMale?: boolean,
              media?: any[],
              registeredAt?: Date) {
    this.id = id;
    this.userRole = role;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.lastName = lastName;
    this.description = description;
    this.telephone = telephone;
    this.password = password;
    this.dateOfBirth = dateOfBirth;
    this.registeredAt = registeredAt;
    this.profileImg = profileImg;
    this.isMale = isMale;
    this.media = media;
    this.fullName = this.lastName + ' ' + this.firstName;
  }

  /**
   * call this method to map the ServerUser to User
   *
   * @param {ServerUser} serverObject
   * @returns {User}
   */
  map(serverObject: ServerUser): User {
    const u = ServerUser.mapReverse(serverObject);
    Object.keys(this).forEach(k => {
      this[k] = u[k];
    });
    return this;
  }

  /**
   * call this method to map the User to ServerUser
   *
   * @returns {ServerUser}
   */
  mapReverse(): ServerUser {
    return ServerUser.map(this);
  }

}
