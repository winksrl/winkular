import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BaseComponent} from './base-component/base.component';
import {BasePageComponent} from './base-page/base-page.component';
import {PlatformLayoutComponent} from './layouts/platform/platform-layout.component';
import {PublicLayoutComponent} from './layouts/public/public-layout.component';
import {FormsModule} from '@angular/forms';
import {ComponentsModule} from '../components/components.module';

/**
 * in this model you can find all base components that group all base methods and attributes.
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    RouterModule,
    NgbModule.forRoot(),
  ],
  declarations: [
    BaseComponent,
    BasePageComponent,
    PlatformLayoutComponent,
    PublicLayoutComponent,
  ],
  exports: [
    BaseComponent,
    BasePageComponent,
    PlatformLayoutComponent,
    PublicLayoutComponent,
  ]
})
export class CoreModule {
}
