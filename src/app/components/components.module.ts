import {NgModule} from '@angular/core';
import {FiltersComponent} from './@shared/filters/filters.component';
import {NavbarComponent} from './navbar/navbar.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {TableComponent} from './@shared/table/table.component';
import {ToolbarComponent} from './@shared/toolbar/toolbar.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgbModalModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PipeModule} from '../pipes/pipe.module';
import {MatIconModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {MediaInputComponent} from './@shared/media-input/media-input.component';
import {MediaModalComponent} from './@shared/media-modal/media-modal.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule.forRoot(),
    PipeModule.forRoot(),
    TranslateModule.forChild({}),
    MatIconModule,
    FormsModule,
    NgbModalModule,
  ],
  declarations: [
    FiltersComponent,
    NavbarComponent,
    SidebarComponent,
    TableComponent,
    ToolbarComponent,
    MediaInputComponent,
    MediaModalComponent
  ],
  exports: [
    FiltersComponent,
    NavbarComponent,
    SidebarComponent,
    TableComponent,
    ToolbarComponent,
    MediaInputComponent,
    MediaModalComponent
  ],
  entryComponents: [MediaModalComponent]
})
export class ComponentsModule {
}
