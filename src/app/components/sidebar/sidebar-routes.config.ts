import {RouteInfo} from './sidebar.metadata';
import {UserRole} from '../../services/session.service';

/**
 * put here every menu voice that you want to show in your sidebar and navbar (for mobile).
 *
 * path {string} the final path
 * title {string} title to show
 * icon {string} associated material icon name (https://material.io/tools/icons/)
 * autorizedUsers {UserRole[]} pust here all type of user that can see the voice
 *
 * @type {[RouteInfo]}
 */
export const SIDEBAR_ROUTES: RouteInfo[] = [
  {path: '/dashboard', title: 'Dashboard', icon: 'home', autorizedUsers: [UserRole.ADMIN, UserRole.EDITOR, UserRole.CUSTOMER]},
  {path: '/user-list', title: 'User list', icon: 'people', autorizedUsers: [UserRole.ADMIN]}
];
