import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatIconModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {CoreModule} from '../../@core/core.module';
import {PipeModule} from '../../pipes/pipe.module';
import {ComponentsModule} from '../../components/components.module';
import {LoginComponent} from './login/login.component';
import {RecoveryPasswordComponent} from './recovery-password/recovery-password.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {PlatformModule} from '../platform/platform.module';
import {RegisterComponent} from './register/register.component';
import {TranslateModule} from '@ngx-translate/core';
import {PolicyComponent} from './policy/policy.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    CoreModule,
    PlatformModule,
    RouterModule,
    NgbModule.forRoot(),
    PipeModule.forRoot(),
    TranslateModule.forChild({}),
    MatIconModule,
  ],
  declarations: [
    LoginComponent,
    RecoveryPasswordComponent,
    RegisterComponent,
    ResetPasswordComponent,
    PolicyComponent
  ],
  exports: [
    LoginComponent,
    RecoveryPasswordComponent,
    RegisterComponent,
    ResetPasswordComponent,
    PolicyComponent
  ]
})
export class PublicModule {
}
