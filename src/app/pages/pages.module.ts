import {NgModule} from '@angular/core';
import {PublicModule} from './public/public.module';
import {PlatformModule} from './platform/platform.module';

@NgModule({
  imports: [
    PlatformModule,
    PublicModule
  ],
  declarations: [],
  exports: []
})
export class PagesModule {
}
