import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatIconModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {DashboardComponent} from './dashboard/dashboard.component';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {UserListComponent} from './user-list/user-list.component';
import {CoreModule} from '../../@core/core.module';
import {PipeModule} from '../../pipes/pipe.module';
import {ComponentsModule} from '../../components/components.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    CoreModule,
    RouterModule,
    NgbModule.forRoot(),
    PipeModule.forRoot(),
    TranslateModule.forChild({}),
    MatIconModule,
  ],
  declarations: [
    DashboardComponent,
    UserDetailComponent,
    UserListComponent,
  ],
  exports: [
    DashboardComponent,
    UserDetailComponent,
    UserListComponent,
  ]
})
export class PlatformModule {
}
