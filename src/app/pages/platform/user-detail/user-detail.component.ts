import {Component, OnInit, OnDestroy, Injector} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../../models/User';
import {BasePageComponent} from '../../../@core/base-page/base-page.component';
import {CustomNotification, CustomNotificationDuration, CustomNotificationType} from '../../../models/ui/CustomNotification';
import {UserRole} from '../../../services/session.service';
import {MediaType} from '../../../models/ui/Media';
import * as moment from 'moment';
import {Utils} from '../../../models/static/Utils';
import {UserService} from '../../../server/services/user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent extends BasePageComponent implements OnInit {

  user: User;
  userFields = [];
  title: string;
  loadingList = {user: false};
  isNew = false;
  isProfile = false;
  userRoles: string[] = Utils.getEnumAsArray(UserRole);
  dateOfBirth: string;
  imageAllowedTypes: MediaType[] = [MediaType.IMAGE, MediaType.PDF];

  constructor(public route: ActivatedRoute,
              protected injector: Injector,
              private userService: UserService) {
    super(injector);
    const id = route.snapshot.paramMap.get('id');
    this.setTitle(id ? (id === 'new' ? 'Create User' : 'User Detail') : 'Profile');
  }

  ngOnInit() {
    super.ngOnInit();
    let id = this.route.snapshot.paramMap.get('id');
    /**
     * check if is profile page or not
     */
    if (!id) {
      this.isProfile = true;
      id = this.loggedinUser.id;
    }
    /**
     * init user, if exists get it, if not create a new one with empty values
     */
    if (id !== 'new') {
      this.loadingList.user = true;
      this.userService.getUserById(id)
        .then(res => this.user = res)
        .then(() => {
          this.userFields = Object.keys(this.user);
          if (this.user.dateOfBirth) {
            this.dateOfBirth = moment(this.user.dateOfBirth).format('yyyy-mm-dd');
          }
          this.loadingList.user = false;
          console.log('User', this.user);
        })
        .catch((error) => {
          console.log(error);
          this.loadingList.user = false;
          this.handleAPIError(error);
        });
    } else {
      this.isNew = true;
      this.user = new User();
      this.userFields = Object.keys(new User());
    }
  }

  /**
   * is form is valid create / update the user
   *
   * @param {User} model
   * @param {boolean} isValid
   */
  submit(model: User, isValid: boolean) {
    console.log(model, isValid);
    if (isValid) {
      this.user.dateOfBirth = moment.utc(this.dateOfBirth).toDate();
      console.log(this.dateOfBirth, this.user.dateOfBirth);
      if (this.isNew) {
        this.user.registeredAt = new Date();
        this.createUser();
      } else {
        this.updateUser();
      }
    }
  }

  async createUser() {
    if (await this.askForConfirmation()) {
      this.loadingList.user = true;
      this.userService.createUser(this.user).then(() => {
        CustomNotification.showNotification(this.toastr, 'User created!', '', CustomNotificationType.SUCCESS, CustomNotificationDuration.SLOW);
        this.loadingList.user = false;
        this.router.navigateByUrl('/user-list');
      }).catch((error) => {
        console.log(error);
        this.loadingList.user = false;
        this.handleAPIError(error);
      });
    }
  }

  async deleteUser() {
    if (await this.askForConfirmation()) {
      this.loadingList.user = true;
      this.userService.deleteUser(this.user.id).then(() => {
        CustomNotification.showNotification(this.toastr, 'User deleted!', '', CustomNotificationType.SUCCESS, CustomNotificationDuration.SLOW);
        this.loadingList.user = false;
        this.router.navigateByUrl('/user-list');
      }).catch((error) => {
        console.log(error);
        this.loadingList.user = false;
        this.handleAPIError(error);
      });
    }
  }

  async updateUser() {
    if (await this.askForConfirmation()) {
      this.loadingList.user = true;
      this.userService.updateUser(this.user).then(() => {
        CustomNotification.showNotification(this.toastr, 'User updated!', '', CustomNotificationType.SUCCESS, CustomNotificationDuration.SLOW);
        this.loadingList.user = false;
      }).catch((error) => {
        this.loadingList.user = false;
        this.handleAPIError(error);
      });
    }
  }
}
