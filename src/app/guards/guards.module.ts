import {NgModule} from '@angular/core';
import {AuthGuard} from './AuthGuard';
import {AdminGuard} from './AdminGuard';

@NgModule({
  imports: [],
  declarations: [],
  exports: [],
  providers: [
    AuthGuard,
    AdminGuard
  ],
})
export class GuardsModule {
}
