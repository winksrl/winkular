'use strict';
const gulp = require('gulp');
const prompt = require('gulp-prompt');
const fs = require('fs');
const color = require('gulp-color');
const exec = require('child_process').exec;

gulp.task('init', () => {
  return new Promise((resolve, reject) => {
    gulp.src('gulpfile.js')
      .pipe(prompt.prompt({
        type: 'list',
        name: 'serverType',
        message: 'What type of server do you want to use?',
        choices: ['strapi', 'firestore'],
        pageSize: '2'
      }, (res) => {
        console.log(color('--- Configuring Winkular for ' + res["serverType"] + '...', 'BLUE'));
        const fileToCheck = 'src/environments/environment.ts';
        if (!fs.existsSync(fileToCheck)) {
          gulp.src(['implementableServers/' + res["serverType"] + '/models/**/*'])
            .pipe(gulp.dest('src/app/server/models/'));
          gulp.src(['implementableServers/' + res["serverType"] + '/services/**/*'])
            .pipe(gulp.dest('src/app/server/services/'));
          gulp.src(['implementableServers/' + res["serverType"] + '/environments/**/*'])
            .pipe(gulp.dest('src/environments/'));
          console.log(color('--- Winkular correctly configured! ---', 'BLUE'));
          console.log(color('--- Installing dependencies...', 'BLUE'));
          console.log(color('--- Dependencies installed! ---', 'BLUE'));
          console.log(color('--- Running server...', 'BLUE'));
          console.log(color('--- ENJOY ---', 'MAGENTA'));
        } else {
          console.log(color('--- Winkular already initialized! ---', 'RED'));
        }
        resolve();
      }))
  })
});
