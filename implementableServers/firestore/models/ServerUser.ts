import {User} from '../../models/User';
import {UserRole} from '../../services/session.service';

export class ServerUser {
  uid?: string;
  id?: string;
  userRole?: string;
  firstName?: string;
  lastName?: string;
  description?: string;
  email?: string;
  telephone?: string;
  profileImg?: string;
  dateOfBirth?: number;
  registeredAt?: number;
  isMale?: boolean;

  /**
   * call this method to map the User to ServerUser
   *
   * @param {} user
   * @returns {ServerUser}
   */
  static map(user: User): ServerUser {
    const u = {} as ServerUser;
    u.id = user.id || null;
    u.uid = user.id || null;
    u.userRole = user.userRole ? user.userRole.toString() : null;
    u.email = user.email || null;
    u.firstName = user.firstName || null;
    u.lastName = user.lastName || null;
    u.description = user.description || null;
    u.telephone = user.telephone || null;
    u.profileImg = user.profileImg || null;
    u.isMale = user.isMale || true;
    u.dateOfBirth = user.dateOfBirth ? user.dateOfBirth.getTime() : null;
    u.registeredAt = user.registeredAt ? user.registeredAt.getTime() : null;
    return u;
  }

  /**
   * call this method to map the ServerUser to User
   *
   * @param {ServerUser} serverObject
   * @returns {}
   */
  static mapReverse(serverObject: ServerUser): User {
    const u = {} as User;
    u.id = serverObject.uid || serverObject.id;
    u.userRole = serverObject.userRole ? UserRole[serverObject.userRole.toUpperCase()] : null;
    u.email = serverObject.email;
    u.firstName = serverObject.firstName;
    u.lastName = serverObject.lastName;
    u.description = serverObject.description;
    u.telephone = serverObject.telephone;
    u.profileImg = serverObject.profileImg;
    u.dateOfBirth = serverObject.dateOfBirth ? new Date(serverObject.dateOfBirth) : null;
    u.registeredAt = serverObject.registeredAt ? new Date(serverObject.registeredAt) : null;
    u.isMale = u.isMale;
    u.fullName = u.lastName + ' ' + u.firstName;
    return u;
  }
}
