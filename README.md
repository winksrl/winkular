# Winkular

This project is a base platform generated with [Angular CLI](https://github.com/angular/angular-cli) that has the goal of generating a backoffice to menage CRUD of dynamic content.

## Get started

- Run `nvm use` tu use the correct version of npm;
- Run `npm run init` to initialize the project:
- Choose the server you want to work with (**Firestore** or **Strapi / Http**)
- Configure your server (check _Configure server_ for details)
- Run `ng serve` to run the server
- Enjoy!

## Configure server

### a. Firestore

If you want to use Winkular with Firestore you must configure before your project from [Firebase](https://console.firebase.google.com/u/0/).

Once created the project open `/src/environments/environment.ts` an update `firebaseConfig` with the project info.

Do the same for `/src/environments/environment.prod.ts` with info for production environment.

### b. Strapi
1. install strapi globally
```
npm install strapi@alpha -g
```

2. Run the following command line in your terminal:
```
strapi new strapi-winkular
```

3. Go to your project and launch the server:
```
cd strapi-winkular
strapi start
```

4. Create your first admin user

5. Open `strapi-winkular/plugins/users-permissions/models/User.settings.json`

6. Replace the content with the follow:

```
{
  "connection": "default",
  "info": {
    "name": "user",
    "description": ""
  },
  "attributes": {
    "username": {
      "type": "string",
      "minLength": 3,
      "unique": true,
      "configurable": false,
      "required": true
    },
    "email": {
      "type": "email",
      "minLength": 6,
      "configurable": false,
      "required": true
    },
    "password": {
      "type": "password",
      "minLength": 6,
      "configurable": false,
      "private": true
    },
    "confirmed": {
      "type": "boolean",
      "default": false,
      "configurable": false
    },
    "blocked": {
      "type": "boolean",
      "default": false,
      "configurable": false
    },
    "role": {
      "model": "role",
      "via": "users",
      "plugin": "users-permissions",
      "configurable": false
    },
    "userRole": {
      "default": "",
      "type": "string"
    },
    "firstName": {
      "default": "",
      "type": "string"
    },
    "lastName": {
      "default": "",
      "type": "string"
    },
    "description": {
      "default": "",
      "type": "string"
    },
    "telephone": {
      "default": "",
      "type": "string"
    },
    "profileImg": {
      "default": "",
      "type": "string"
    },
    "dateOfBirth": {
      "default": "",
      "type": "integer"
    },
    "registeredAt": {
      "default": "",
      "type": "integer"
    },
    "isMale": {
      "default": false,
      "type": "boolean"
    },
    "media": {
      "collection": "file",
      "via": "related",
      "plugin": "upload"
    }
  },
  "collectionName": "users-permissions_user"
}

```

7. Go in `http://localhost:1337/admin/plugins/content-manager/user?source=users-permissions`

8. Open the detail of the admin, assign at userRole the value `ADMIN` and save.

9. Now you can log into Winkular using this user credentials!

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:5000/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
